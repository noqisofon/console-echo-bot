import asyncio

from adapter  import ConsoleAdapter
from bot      import EchoBot

# アダプターを作成します。
ADAPTER  = ConsoleAdapter()
BOT      = EchoBot()

LOOP     = asyncio.get_event_loop()

if __name__ == '__main__':
    try:
        # ユーザーに挨拶します。
        print( 'こんにちは。私はエコー Bot です。あなたが言ったことを返します。' )
        print( '「quit」と言ったら、セッションを終了します。' )

        LOOP.run_until_complete( ADAPTER.process_activity( BOT.on_turn ) )
    except KeyboardInterrupt:
        pass
    finally:
        LOOP.stop()
        LOOP.close()
