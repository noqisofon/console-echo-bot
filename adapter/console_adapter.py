import asyncio
import datetime
from typing                       import List, Callable
import warnings

from botbuilder.core.turn_context import TurnContext
from botbuilder.core.bot_adapter  import BotAdapter
from botbuilder.schema            import (
    Activity,
    ActivityTypes,
    ChannelAccount,
    ConversationAccount,
    ConversationReference,
    ResourceResponse
)

class ConsoleAdapter(BotAdapter):
    """
    ユーザーがコンソールウィンドウから Bot と通信できるようにします。

    :Example:
    import asyncio
    from botbuilder.core import ConsoleAdapter

    async def logic(context):
        await context.send_activity( 'Hello, World!' )

    adapter = ConsoleAdapter()
    loop    = asyncio.get_event_loop()

    if __name__ == '__main__':
        try:
            loop.run_until_complete( adapter.process_activity( logic ) )
        except KeyboardInterrupt:
            pass
        finally:
            loop.stop()
            loop.close()
    """

    def __init__(self, reference: ConversationReference = None):
        """
        """
        super( ConsoleAdapter, self ).__init__()

        self.reference = ConversationReference(
            channel_id='console',
            user=ChannelAccount(id='user', name='User1'),
            conversation=ConversationAccount(id='convo1', name='', is_group=False),
            service_url=''
        )

        if reference is not None and not isinstance( reference, ConversationReference ):
            # ConversationReference のインスタンスを渡すようにユーザーに警告します。
            # そうしないと、パラメーターが無視されます。
            warnings.warn(
                "ConsoleAdapter: `reference` argument is not an instance of ConversationReference and will "
                "be ignored."
            )
        else:
            self.reference.channel_id   = getattr( reference, 'channel_id'  , self.reference.channel_id )
            self.reference.user         = getattr( reference, 'user'        , self.reference.user )
            self.reference.bot          = getattr( reference, 'bot'         , self.reference.bot )
            self.reference.conversation = getattr( reference, 'conversation', self.reference.conversation )
            self.reference.service_url  = getattr( reference, 'service_url' , self.reference.service_url )
            # 初期値のない self.reference の唯一の属性は activity_id であるため、reference に activity_id の値が None
            # の場合、デフォルトの self.reference.activity_id の値は None になります。
            self.reference.activity_id  = getattr( reference, 'activity_id' , None )

        self._next_id = 0

    async def process_activity(self, logic: Callable):
        """
        コンソール入力の聞き耳を立てます。

        :param logic:
        :return
        """
        while True:
            message = input()

            if message is None:
                pass
            else:
                self._next_id += 1
                activity      = Activity(
                    text=message,
                    channel_id='console',
                    recipient=ConversationAccount(id='Convo1'),
                    type=ActivityTypes.message,
                    timestamp=datetime.datetime.now(),
                    id=str( self._next_id )
                )

                activity = TurnContext.apply_conversation_reference(
                    activity,
                    self.reference,
                    True
                )
                context  = TurnContext( self, activity )
                await self.run_pipeline( context, logic )

    async def send_activities(self, context: TurnContext, activities: List[Activity]) -> List[ResourceResponse]:
        """
        一連のアクティビティをコンソールに記録します。

        :param context:
        :param activities:
        :return:
        """
        if context is None:
            raise TypeError( "ConsoleAdapter#send_activities(): `context` argument cannot be None." )
        if not isinstance( activities, list ):
            raise TypeError( "ConsoleAdapter#send_activities(): `activities` argument must be a list." )
        if len( activities ) == 0:
            raise ValueError( "ConsoleAdapter#send_activities(): `activities` argument cannot have a length of 0." )

        async def next_activity(an_index: int):
            responses = []

            if an_index < len(activities):
                responses.append( ResourceResponse() )
                activity = activities[an_index]

                if activity.type == 'delay':
                    await asyncio.sleep( activity.delay )
                    await next_activity( an_index + 1 )
                elif activity.type == ActivityTypes.message:
                    if ( activity.attachments is not None
                         and len( activity.attachments ) > 0):
                        appendee = (
                            '(1 attachment)'
                            if len( activity.attachments ) == 1
                            else '({} attachment)'.format(  len( activity.attachments ) )
                        )

                        print( '{} {}'.format( activity.text, appendee ) )
                    else:
                        print( activity.text )
                    await next_activity( an_index + 1 )
                else:
                    print( '[{}]'.format( activity.type ) )

        await next_activity( 0 )

    async def delete_activity(self, context: TurnContext, reference: ConversationReference):
        """
        ConsoleAdapter ではサポートされていません。
        このメソッドまたは `TurnContext.delete_activity()` を呼び出すと、エラーが返されます。 

        :param context:
        :param reference:
        :return:
        """
        raise NotImplementedError( 'ConsoleAdapter#delete_activity(): not supported.' )

    async def update_activity(self, context: TurnContext, activity: Activity):
        """
        ConsoleAdapter ではサポートされていません。
        このメソッドまたは `TurnContext.update_activity()` を呼び出すと、エラーが返されます。 

        :param context:
        :param acitivities:
        :return:
        """
        raise NotImplementedError( 'ConsoleAdapter#update_actibity(): not supported.' )
