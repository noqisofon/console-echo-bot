console-echo-bot
==============================================================================

これは [microsoft/BotBuilder-Samples](https://github.com/microsoft/BotBuilder-Samples) の [01.console-echo](https://github.com/microsoft/BotBuilder-Samples/tree/main/samples/python/01.console-echo) を目コピしたもので、なんら目新しいものはありません。  
端末エミュレーター上で動くエコー Bot です。

## 使い方

    $ pip install -r ./requirements.txt
    $ python ./main.py

## 必要なもの

- python 3.8.7

必要なパッケージは `requirements.txt` を見てください。
