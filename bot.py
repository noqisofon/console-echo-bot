from sys import exit

class EchoBot:
    """
    """
    async def on_turn(self, context):
        """
        """
        # このアクティビティが着信メッセージであるかどうかを確認します。
        # (理論的には別の種類のアクティビティである可能性があります)
        if context.activity.type == 'message' and context.activity.text:
            # ユーザーが単純な「終了」メッセージを送信したかどうかを確認します。
            if context.activity.text.lower() == 'quit':
                # リプを返します。
                await context.send_activity( 'またよろしくお願いします。' )
                exit( 0 )
            else:
                # メッセージテキストをエコーしてユーザーに戻します。
                await context.send_activity( 'あなたが「{}」と言うのを聞きました。'.format( context.activity.text ) )
